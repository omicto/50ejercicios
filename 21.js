function sumEvensUpTo(max){
    let sum = 0;
    for(let i = 0; i <= max; i++){
        if(!(i & 1)){
            sum += i;
        }
    }
    return sum;
}

function sumOddsUpTo(max){
    let sum = 0;
    for(let i = 0; i <= max; i++){
        if(i & 1){
            sum += i;
        }
    }
    return sum;
}

console.log(`Suma pares hasta 1000: ${sumEvensUpTo(1000)}`);
console.log(`Suma impares hasta 1000: ${sumOddsUpTo(1000)}`);