const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: true
});

rl.on('line', (line) => {
    let loweredLine = line.toLowerCase();
    if(!(loweredLine === 's' || loweredLine === 'n')){
        console.log('Not allowed');
        rl.close();
    }
});