class Data{
    constructor(id, nombre, apellidos, direccion, estado){
        this.id = id;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.direccion = direccion;
        this.estado = estado;
    }
}

module.exports = Data;