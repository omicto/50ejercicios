const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: true
});

rl.question("Ingresa un numero: ", (ans) => {
    let num = parseInt(ans);
    let count = 0;
    if(num > 0) {
        count = Math.floor(num/3);
        console.log(count);
    }
    rl.close();
});