const fs = require('fs');
class DatosSaver{
    constructor(fileName){
        this.file = fs.openSync(fileName, 'r+');
        this.contents = JSON.parse(fs.readSync(this.file));
    }

    saveData(data){
        this.contents.push(data);
        fs.writeSync(this.file, this.contents);
    }

    get allData(){
        return this.contents;
    }

    removeRecord(id){
        this.contents = this.contents.filter((record) => record.id !== id);
        fs.writeSync(this.file, this.contents); 
    }

    updateRecord(id, newData){
        this.removeRecord(id);
        newData.id = id;
        this.saveData(newData);
    }

    getById(id){
        return this.contents.filter(record => record.id === id);
    }
}
module.exports = DatosSaver;