const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: true
});
function romanizar(n) {
    let romanos = [
        ['M', 1000],
        ['CM', 900],
        ['D', 500],
        ['CD', 400],
        ['C', 100],
        ['XC', 90],
        ['L', 50],
        ['XL', 40],
        ['X', 10],
        ['IX', 9],
        ['V', 5],
        ['IV', 4],
        ['I', 1]
    ]

    let romanizado = "";
    for (let i = 0; i < romanos.length; i++) {
        while (romanos[i][1] <= n) {
            romanizado += romanos[i][0];
            n -= romanos[i][1];
        }
    }

    return romanizado;
}

rl.question("Romanizar un numero menor a 5000: ", (ans) => {
    let n = Number(ans);
    console.log(romanizar(n));
    rl.close();
});
