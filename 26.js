const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: true
});

let x = Math.floor(process.stdout.columns/2);
let y = Math.floor(process.stdout.rows/2);

let pad = "\n".repeat(y) + " ".repeat(x);

rl.question("Ingresa una frase: ", (frase) => {
    console.log(pad + frase);
    rl.close();
});