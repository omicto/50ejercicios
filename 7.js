const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: true
});

let frases = []

rl.on('line', (line) => {
    frases.push(line);
});

rl.on('close', () => {
    console.log(`Numero total de frases introducidas ${frases.length}`);
});