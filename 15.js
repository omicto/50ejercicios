const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: true
});
let min = Math.min;
let max = Math.max;

let a;
let b;

rl.question("Ingrese un numero: ", (ans) => {
    a = Number(ans);
    rl.question("Ingrese otro numero: ", (ans) => {
        b = Number(ans);
        let {cuentaPares, sumaImpares} = naturales(min(a,b), max(a,b));
        console.log(`C. pares: ${cuentaPares}`);
        console.log(`Sum. impares: ${sumaImpares}`);
        rl.close();
    });
});

function naturales(start, end){
    start = Math.ceil(start);
    end = Math.floor(end);
    let count = 0;
    let sum = 0;
    for(let i = start; i <= end; i++){
        if(i & 1){
            sum += i;
        }else{
            count++;
        }
        console.log(i);
    }
    return {
        sumaImpares: sum,
        cuentaPares: count
    };
}

