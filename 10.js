const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: true
});

rl.question('Ingresa un numero: ', (ans) => {
    let n = Number(ans);
    console.log((n & 1) ? 'Impar' : 'Par');
    rl.close();
});