const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: true
});

rl.question("Ingrese una frase: ", (frase) => {
    rl.question("Contar el numero de veces que ocurre la letra: __ ", (l) => {
        let partes = frase.split(l);
        console.log(`Ocurrencias de ${l}: ${partes.length - 1}`);
        rl.close();
    });
});