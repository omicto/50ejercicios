const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: true
});

rl.question('Ingrese una frase: ', (line) => {
    for(let i = 0; i < 5; i++){
        console.log('    '.repeat(i) + line);
    }
    rl.close();
});