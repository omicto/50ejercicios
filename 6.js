const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: true
});

rl.question("Ingresa un numero", (ans) => {
    let num = parseInt(ans);
    if(num > 0) {
        for(let i = 0; i < num; i++){
            console.log(i);
        }
    }
    rl.close();
});
