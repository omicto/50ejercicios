const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: true
});

function factorial(n){
    if(n === 0 || n === 1){
        return 1;
    }
    return n * factorial(n-1);
}

rl.question("Calcular el factorial de: ", (n) => {
    n = Number(n);
    console.log(`El resultado es ${factorial(n)}`);
    rl.close();
});
