const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: true
});

rl.question("Tabla de multiplicar de: ", (numero) => {
    for(let i = 1; i <= 10; i++){
        console.log(`${numero}*${i} = ${numero*i}`);
    }
    rl.close();
});