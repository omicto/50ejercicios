const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: true
});

console.log('Ingrese 5 numeros');
let numbers = [];
rl.on('line', (line) => {
    numbers.push(Number(line));
    if(numbers.length === 5){
        console.log(`Maximo: ${Math.max(...numbers)}`);
        console.log(`Minimo: ${Math.min(...numbers)}`);
        rl.close();
    }
});